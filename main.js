document.getElementById('add-task-btn').addEventListener('click', function() {
  const taskInput = document.getElementById('task-input');
  const taskList = document.getElementById('task-list');

  if (taskInput.value.trim() !== '') {
    const li = document.createElement('li');
    li.className = 'flex justify-between items-center p-2 rounded bg-white shadow';

    const span = document.createElement('span');
    span.textContent = taskInput.value;
    li.appendChild(span);

    const btn = document.createElement('button');
    btn.innerHTML = '<i class="fas fa-trash text-red-500"></i>';
    btn.addEventListener('click', function() {
      taskList.removeChild(li);
    });
    li.appendChild(btn);

    taskList.appendChild(li);
    taskInput.value = '';
  }
});
